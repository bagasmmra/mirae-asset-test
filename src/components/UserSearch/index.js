import React, {useEffect, useState} from 'react';
import {Input, List} from 'antd';

import './index.css';

const {Search} = Input;

const UserSearch = () => {
    const [search, setSearch] = useState('');
    const [users, setUsers] = useState([]);

    useEffect(() => {
        const fetchUsers = async () => {
            if (search) {
                try {
                    const response = await fetch(`https://api.github.com/search/users?q=${search}`, {
                        headers: {
                            'Accept': 'application/vnd.github.v3+json',
                        },
                    });
                    if (response.ok) {
                        const data = await response.json();
                        setUsers(data.items);
                    } else {
                        console.error('Failed to fetch users');
                    }
                } catch (error) {
                    console.error('Network error', error);
                }
            } else {
                setUsers([]);
            }
        };

        fetchUsers();
    }, [search]);

    return (<div>
            <Search
                placeholder="Search GitHub users"
                value={search}
                onChange={(e) => setSearch(e.target.value)}
                style={{width: 300, marginBottom:20}}
            />
            <div className="user-search-container">
                <List
                    dataSource={users}
                    renderItem={(user) => (<List.Item className="user-list-item">
                            {user.login}
                        </List.Item>)}
                />
            </div>
        </div>);
};

export default UserSearch;