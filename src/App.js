import React from 'react';
import './App.css';
import UserSearch from './components/UserSearch';

function App() {
  return (
      <div className="App">
        <h1>Github User Search</h1>
        <UserSearch />
      </div>
  );
}

export default App;